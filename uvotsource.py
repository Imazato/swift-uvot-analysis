import glob
from tqdm import tqdm
import subprocess
from astropy.io import fits

def perform_uvotsource(img_file, exp_file, src_reg, bgd_reg, output=False):
    """Perform the uvotsource command

    Args:
        img_file: UVOT FITS file. ex) sw00030354002ubb_sk.img.gz
        src_reg: source region file
        bgd_reg: back ground region file
        exp_file: exposure map file
        output_file: output FITS file to append results to.
    """
    hdu    = fits.open(img_file)
    ID     = hdu[0].header['OBS_ID']
    FILTER = hdu[0].header['FILTER']
    for i, in_file in enumerate(hdu[1:]):
        i=i+1
        if output == True:
            output_fits = 'uvotsource_' + ID + '_' + FILTER + '_+' + str(i) + '.fits'
        else:
            output_fits = None

        uvotsource_cmd = 'uvotsource image=' + str(img_file) + '+' + str(i) + ' srcreg=' + src_reg + ' bkgreg=' + bgd_reg + ' expfile=' + exp_file + ' outfile=' + output_fits + ' sigma=6  syserr=yes cleanup=yes clobber=yes'
        subprocess.run(uvotsource_cmd, shell=True)


if __name__ == '__main__':
    img_files = glob.glob('reproc/000*/uvot/image/sw000*u??_sk.img.gz')
    exp_files = glob.glob('reproc/000*/uvot/image/sw000*u??_ex.img.gz')

    src_reg = 'src.reg'
    bgd_reg = 'bgd.reg'

    for img_file, exp_file in tqdm(zip(img_files, exp_files)):
        perform_uvotsource(img_file=img_file, exp_file=exp_file, src_reg=src_reg, bgd_reg=bgd_reg, output=True)

# src reg example
"""
# Region file format: DS9 version 4.1
global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1
fk5
circle(3:19:48.1598,+41:30:42.114,5.020") # text={NGC 1275}
"""
# bgd reg example
"""
# Region file format: DS9 version 4.1
global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1
fk5
circle(3:19:46.0816,+41:29:28.065,20.080") # text={bgd}
"""