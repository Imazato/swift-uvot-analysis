# Swift UVOT analysis

For analysis Swift UVOT data

#### step0 (clusterでやる人用)  
環境設定を行うファイル /cluster521/users/imazato/swift/test6/setup_headas_cluster49.sh をコピーして実行してください。  

#### step1  
uvotsource.pyをgit clone。  
ここからSwiftデータダウンロード: https://www.swift.ac.uk/swift_live/index.php  
オブジェクト名を打ったらたくさんデータが出てくるので欲しい時期のデータをダウンロードする。  
ダウンロードしたら、ダウンロードしたディレクトリの下にreprocのディレクトリができる。  

#### step2  
uvotsource.pyをreprocと同じディレクトリに置く(もし嫌なら、uvotsource.pyをいじって自由にしてください)。  

#### step3  
uvotsource.pyの中身に必要情報を書く。下に書きます。  
1. image file(ex. reproc/00081530001/uvot/image/sw00081530001uw2_sk.img.gz)とexposure map(ex. reproc/00081530001/uvot/image/sw00081530001uw2_ex.img.gz)のデータの場所  
2. source regionとbackground regionの場所(uvotsource.pyの下にregionファイルの例を書きました。I澤くんは分からないと思うので平手さんかまたけさんに作り方を聞いてください)  
3. output名を指定する。何も指定しなかったら出力ファイルは uvotsource_00081530001_UVW2_+1.fits のようになる。

#### step4  
uvotsource.py回す。  

#### step5  
output fileにMagnitudeやフラックスなどが書いてあるから自分の欲しい情報を抜き出して自由に使ってください。  

#### step6  
uvotsource.pyを改良してPull requestしてください。